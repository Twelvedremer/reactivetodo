//
//  Task.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Task{
    var name: String!
    var priority: Int!
    var dueDate: Date!
    
    var isOverDue: Bool{
        if dueDate >= Date(){
            return false
        }
        return true
    }
    
    init(){
        name = ""
        priority = 1
        dueDate = Date()
    }
    
    init?(_ data: JSON) {
        guard let name = data["name"].string,
             let priority = data["priority"].int,
             (1...5).contains(priority),
             let dueDate = data["dueDate"].date else {
                    return nil
        }
        
        self.name = name
        self.priority = priority
        self.dueDate = dueDate
    }
    
}
