//
//  UIViewController+Extensions.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import Foundation
import RxSwift

extension UIViewController{
    
    func presentAlert(title: String, content: String?) -> Observable<Void>{
        return Observable.create({ [weak self] observable in
            let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default,
                                          handler: { _ in
                                            observable.onCompleted()
            }))
            self?.present(alert, animated: true)
            return Disposables.create {
                self?.dismiss(animated: true)
            }
        })
    }
}
