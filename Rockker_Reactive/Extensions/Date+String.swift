//
//  Date+String.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import Foundation

extension Date{
    
    var string: String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: self)
    }
}
