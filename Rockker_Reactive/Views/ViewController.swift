//
//  ViewController.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var selectType: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    private let bag = DisposeBag()
    var viewModel = TodoServiceViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.selectedTask.asObservable().subscribe({ [weak self] _ in
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }).disposed(by: bag)
        
        selectType.rx.value
            .map{ type -> Bool in
                    return type == 1 }
            .bind(to: viewModel.isOverDue)
            .disposed(by: bag)
        
        viewModel.getTask()
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? FilterViewController{
            destination.viewModel = viewModel
        } else if let destination = segue.destination as? NewTaskViewController{
            destination.viewModel = viewModel
        }
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.selectedTask.value.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoCell", for: indexPath) as! TaskTableViewCell
        let task = viewModel.selectedTask.value[indexPath.row]
        cell.loadData(task)
        cell.selectionStyle = .none
        return cell
    }
    
}

