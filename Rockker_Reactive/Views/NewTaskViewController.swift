//
//  NewTaskViewController.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NewTaskViewController: UIViewController {

    @IBOutlet weak var PriorityNumber: UILabel!
    @IBOutlet weak var prioritySlide: UISlider!
    @IBOutlet weak var dueDateField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var taskButtom: UIButton!
    var viewModel: TodoServiceViewModel!
    private let bag = DisposeBag()
    
    private var dateString = Variable<String?>("")
    private var nameString = Variable<String?>("")
    private var statusBottom = Variable<Bool>(false)
    private var selectedDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        prioritySlide
            .rx.value
            .map{ "\(Int($0))" }
            .bind(to: PriorityNumber.rx.text)
            .disposed(by: bag)
        
        nameField
            .rx.text
            .map{$0}
            .bind(to: nameString)
            .disposed(by: bag)
        
        dueDateField
            .rx.text
            .map{$0}
            .bind(to: dateString)
            .disposed(by: bag)
        
        Observable
            .combineLatest(dateString.asObservable(), nameString.asObservable()){ (date, name) in
                return !(date?.isEmpty ?? true) && !(name?.isEmpty ?? true) }
            .bind(to: statusBottom)
            .disposed(by: bag)
        
        statusBottom.asObservable().subscribe(onNext: { status in
            self.taskButtom.backgroundColor = status ? .blue : .gray
            self.taskButtom.isEnabled = status
        }).disposed(by: bag)
    
        statusBottom.value = false
    }
    
    @IBAction func textFieldEditing(_ sender: UITextField) {
        let datePickerView: UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.date = selectedDate ?? Date()
        sender.inputView = datePickerView
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton =  UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
        
        doneButton.rx
            .tap.asObservable()
            .subscribe(onNext: {
                self.selectedDate = datePickerView.date
                sender.resignFirstResponder()})
            .disposed(by: bag)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        sender.inputAccessoryView = toolBar
        
        
        datePickerView.rx.value
            .map { dateValue -> String in
                return dateValue.string}
            .bind(to: sender.rx.text)
            .disposed(by: bag)
    }
   
    @IBAction func createdTask(_ sender: Any) {
        guard let date = dateString.value, let name = nameString.value  else {
            self.presentAlert(title: "", content: "Error empty field").subscribe().disposed(by: self.bag)
            return
        }
        viewModel
            .createTask(name: name, date: date, priority: Int(prioritySlide.value))
            .subscribe(onNext:{ status in
            if status{
                self.presentAlert(title: "", content: "New task Created").subscribe().disposed(by: self.bag)
                self.nameField.text = ""
                self.dueDateField.text = ""
                self.selectedDate = nil
                self.prioritySlide.value = 1.0
            } else {
                self.presentAlert(title: "", content: "Server Error").subscribe().disposed(by: self.bag)
            }})
            .disposed(by: bag)
        
    }
    
    @IBAction func ringVolumeSliderChange(_ sender: UISlider){
        sender.setValue(sender.value.rounded(.down), animated: true)
    }
    
    
}
