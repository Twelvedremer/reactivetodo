//
//  FilterViewController.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit
import RxSwift

class FilterViewController: UIViewController {

    @IBOutlet weak var orderingFilter: UISegmentedControl!
    @IBOutlet weak var SortAsc: UISwitch!
    var viewModel: TodoServiceViewModel!
    private let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        orderingFilter.selectedSegmentIndex = viewModel.ordering.value
        SortAsc.isOn = viewModel.ascSort.value
        
        orderingFilter.rx
            .selectedSegmentIndex
            .bind(to: viewModel.ordering)
            .disposed(by: bag)
        
        SortAsc.rx.isOn
            .bind(to: viewModel.ascSort)
            .disposed(by: bag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
