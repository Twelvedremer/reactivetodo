//
//  TaskTableViewCell.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var priority: UILabel!
    @IBOutlet weak var dueDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
    
    func loadData(_ task: Task){
        self.name.text = task.name
        if let Numpriority = task.priority{
            self.priority.text = "priority \(Numpriority)"
        }
        self.dueDate.text = task.dueDate.string
        if task.isOverDue{
            self.name.textColor = .red
        } else {
            self.name.textColor = .green
        }
    }

}
