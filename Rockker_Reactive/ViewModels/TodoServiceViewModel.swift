//
//  TodoServiceViewModel.swift
//  Rockker_Reactive
//
//  Created by Momentum Lab 1 on 1/23/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

class TodoServiceViewModel{
    
    // MARK: public properties
    var isOverDue = Variable<Bool>(false)
    var ordering = Variable<Int>(0)
    var ascSort = Variable<Bool>(true)
    var selectedTask = Variable<[Task]>([])
    
    // MARK: private properties
    private let bag = DisposeBag()
    private var task = Variable<[Task]>([])
    private let url = "http://r3l-todo-list.herokuapp.com/"
    
    init() {
        Observable.combineLatest(task.asObservable(), isOverDue.asObservable()){ (task, type) -> [Task] in
            return task
                .filter { taskFilter in
                return type ? taskFilter.isOverDue : !taskFilter.isOverDue}
                .sorted{
                    switch self.ordering.value{
                        case 0: return self.ascSort.value ? $0.dueDate <= $1.dueDate : $0.dueDate > $1.dueDate
                        case 1: return self.ascSort.value ? $0.name <= $1.name : $0.name > $1.name
                        default: return self.ascSort.value ? $0.priority <= $1.priority : $0.priority > $1.priority
                    }
                }}
                .bind(to: selectedTask).disposed(by: bag)
        
        Observable.combineLatest(ordering.asObservable(), ascSort.asObservable()){ (ordering, asSort) -> [Task] in
            return self.task.value.filter { taskFilter in
                return self.isOverDue.value ? taskFilter.isOverDue : !taskFilter.isOverDue}
                .sorted{
                    switch ordering{
                    case 0: return asSort ? $0.dueDate <= $1.dueDate : $0.dueDate > $1.dueDate
                    case 1: return asSort ? $0.name <= $1.name : $0.name > $1.name
                    default: return asSort ? $0.priority <= $1.priority : $0.priority > $1.priority
                    }
            }
        }.bind(to: selectedTask).disposed(by: bag)
        
    }

    public func getTask(){
        let response = Observable.of(url + "task/")
            .map { urlString -> URL in
                return URL(string: urlString)!}
            .flatMap { request -> Observable<Any> in
                return URLSession.shared.rx.json(url: request)}
            .map{ json -> [JSON] in
                let json = JSON(json)
                guard let jsonObject = json.array else{
                    return []
                }
                return jsonObject}
            .map { (jsonList) -> [Task] in
                return jsonList.flatMap{Task($0)}
            }
        
        response.bind(to: task).disposed(by: bag)
    }
    
    public func createTask(name: String, date: String, priority: Int) -> Observable<Bool>{
        let response = Observable.of(url + "task/create?name=\(name)&dueDate=\(date)&priority=\(priority)")
            .map { urlString -> URL in
                return URL(string: urlString)!}
            .map { urlRquest -> URLRequest in
                var request = URLRequest(url: urlRquest)
                request.httpMethod = "POST"
                return request }
            .flatMap { request -> Observable<Any> in
                return URLSession.shared.rx.json(request: request)}
            .map{ json -> Task? in
                let json = JSON(json)
                guard let task = Task(json) else{
                    return nil
                }
                return task}
            .map { (task) -> Bool in
                guard let task = task else{
                    return false
                }
                self.task.value.append(task)
                return true}
        
        return response
        }
    
}
